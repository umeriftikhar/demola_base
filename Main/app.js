﻿// server.js

// set up ======================================================================
// get all the tools we need
var express = require('express');
var app = express();

var http = require('http');
var server = http.createServer(app);
var io = require('socket.io').listen(server);

var port = process.env.PORT || 8080;
var mongoose = require('mongoose');
var passport = require('passport');
var flash = require('connect-flash');
var mailer = require("nodemailer");

var morgan = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
//var session = require('express-session');

var configDB = require('./config/database.js');
app.use('/js', express.static(__dirname + '/client/js'));
app.use('/styles', express.static(__dirname + '/public/stylesheets'));
// configuration ===============================================================
//mongoose.connect(configDB.url); // connect to our database
var options = {
    db: { native_parser: true },
    server: { poolSize: 5 },
    replset: { rs_name: 'myReplicaSetName' },
    user: 'admin',
    pass: 'admin'
};
mongoose.connect(configDB.url, options);
 require('./config/passport')(passport); // pass passport for configuration

// set up our express application
app.use(morgan('dev')); // log every request to the console
app.use(cookieParser()); // read cookies (needed for auth)
app.use(bodyParser()); // get information from html forms

//app.set('view engine', 'ejs'); // set up ejs for templating

// required for passport
app.use(express.session({ secret: 'ilovescotchscotchyscotchscotch' })); // session secret
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(flash()); // use connect-flash for flash messages stored in session



// routes ======================================================================
require('./app/routes.js')(app, passport); // load our routes and pass in our app and fully configured passport

// launch ======================================================================



io.on("connection", function (socket) {
    var tweet = {user: "nodesource", text: "Testing Server To Client Communication!"};

	function addLeadingZero(num){
                   return (num <= 9)? ("0"+num) : num;
               }
			   

									
    // to make things interesting, have it send every second
    var interval = setInterval(function () {
        //socket.emit("tweet", {"firstName":"John", "lastName":"Doe"});
	                    var currDate = new Date();
                        var label = addLeadingZero(currDate.getHours()) + ":" + addLeadingZero(currDate.getMinutes()) + ":" + addLeadingZero(currDate.getSeconds());
                        // Get random number between 35.25 & 35.75 - rounded to 2 decimal places
                        var randomValue = Math.floor(Math.random() * 50) / 100 + 35.25;
                        // Build Data String in format &label=...&value=...
                        var strData = "&label=" + label + "&value=" + randomValue;

		socket.emit("tweet", strData);
    }, 1000);

    socket.on("disconnect", function () {
        clearInterval(interval);
    });
}); 







//app.listen(port);
server.listen(port);
console.log('The magic happens on port ' + port);